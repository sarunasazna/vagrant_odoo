#!/bin/bash 
# get Odoo 8
wget -O - https://nightly.odoo.com/odoo.key | apt-key add -
echo "deb http://nightly.odoo.com/8.0/nightly/deb/ ./" >> /etc/apt/sources.list
# update packages
sudo apt-get update
sudo apt-get upgrade 

# Install packages
sudo apt-get -y  --force-yes install debconf-utils less lsof screen vim git postgresql zsh
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
sudo su postgres -c "createuser -d -s -r --replication odoo"
sudo apt-get -y  --force-yes install odoo
# Move odoo service files to shared folder
sudo service odoo stop
# Copy initial conf file from shared folder to odoo path
sudo rm /etc/odoo/openerp-server.conf
sudo cp /home/vagrant/odoo_service/openerp-server-initial.conf /etc/odoo/openerp-server.conf
sudo chown -R odoo:odoo /etc/odoo/openerp-server.conf
sudo service odoo start
