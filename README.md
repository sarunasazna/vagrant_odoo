vagrant-odoo8
===========

This repo contains provisioning scripts for odoo8 (community version) vagrant vm. Provisioning is done by *script.sh*.

# Vagrant installation (Debian/Ubuntu)

```bash
# install Virtualbox, Vagrant and dkms(to ensure that the VirtualBox host kernel modules).
sudo apt-get install virtualbox vagrant virtualbox-dkms
```
# Provisioning
```bash
# provision the virtual machine
vagrant up
```

# Reprovisioning
```bash
# Execute this command if script.sh, Vagrantfile or other dependencies have changed
vagrant provision
```

# Connecting to vagrant
First connect to the machine via ssh (password: vagrant):
```bash
vagrant ssh
```
After that with superuser:
```bash
sudo su
```
Home directory(*/home/vagrant*) is shared with your local machine(*./vagrant_odoo8*).

# Working with Odoo
Conf file path: */etc/odoo/openerp-server.conf*

Log file path: */var/log/odoo/odoo-server.log*

URL: *http://127.0.0.1:8069/*

Odoo service manipulation:
```bash
sudo service odoo start/restart/stop
```
